//
//  PamCollectionViewCell.swift
//  ema
//
//  Created by Adam Preston on 4/29/16.
//  Copyright © 2016 RTI. All rights reserved.
//

import UIKit

class PamCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}
